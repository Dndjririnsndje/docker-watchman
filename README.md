# docker-watchman

Watchman is a file watching service developed by Facebook, and it's used by many projects (like Ember CLI) to watch 
files for changes, triggering project rebuilds whenever a change is detected.

## How to build

Install qemu with arm support

```shell
$ sudo apt install qemu-system-arm
```

You need install [docker buildx](https://github.com/docker/buildx/#installing)

To push images to repo you need configure credentials.

[comment]: <> (# TODO: How to configure credentials )

Now you can build images for you local use:

```shell
$ make
```

Or run deploy to production (versions are hardcoded for now):

```shell
$ make push
```

## How to use

Put in your Dockerfile

```dockerfile

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libssl1.1 \
    && rm -rf /var/lib/apt/lists/*
COPY --from=registry.gitlab.com/mrmilu/public-docker-images/docker-watchman:4.9.0 /usr/local/bin/watchman* /usr/local/bin/
# Create the watchman STATEDIR directory:
RUN mkdir -p /usr/local/var/run/watchman && touch /usr/local/var/run/watchman/.not-empty

```